# APIRouter <sup>[1](#footnote_1)</sup>

앱이 복잡해지면 관리와 유지보수가 더 어려워질 수 있다. 개발자가 직면하는 일반적인 문제 중 하나는 코드베이스의 구성이다. 작은 앱에서는 모든 컨트롤러를 `main.py`와 같은 단일 파일에 보관하는 것으로 충분할 때가 많다. 그러나 앱이 성장함에 따라 이러한 접근 방식은 유지 관리가 어려워질 수 있다.

또 다른 문제는 API의 버전 관리와 엔드포인트 그룹화이다. `api/v1/`과 같은 엔드포인트 접두사를 반복하면 오류가 발생하기 쉽다.

> 이 포스팅은 [이전 포스팅](./auth.md)의 후속 글이다. 아직 읽어보지 않았다면 더 나은 맥락과 이해를 위해 읽기를 적극 권장한다.

우리 목표는 목적에 따라 컨트롤러를 그룹화하는 것이다. 예를 들어, 버전 관리 없이 4개의 엔드포인트를 포함하는 CRUD 작업을 할 수 있다.

응답이나 요청 구조를 변경하는 것은 좋은 습관이 아니기 때문에 REST API에는 버전 관리가 필수적이다. 이는 소비자(프론트엔드)를 쉽게 중단시킬 수 있다. 대신 엔드포인트의 새 버전을 만들고 프런트엔드가 새 버전으로 마이그레이션할 때까지 이전 버전을 지원할 수 있다.

> Get all todos: [GET] /api/v1/todos
> 
> Get a todo by id: [GET] /api/v1/todos/:id
> 
> Create a todo: [POST] /api/v1/todos
> 
> Delete todo: [DELETE] /api/v1/todos/:id

여기서 몇 가지 유사점이 보이나요? `/api/v1/todos`이 반복된다. 이것이 바로 그룹화라는 의미이다.

## 폴더 + 파일
먼저 폴더 `controller`와 그 안에 `authController.p`y와 `todoController.py` 두 파일을 만들어야 한다.

```
todo-list
├── app
...
│   ├── auth
│   │   └── __init__.py
│   │   └── password.py
│   │   └── token.py
│   │   └── user.py
│   ├── controllers
│   │   └── __init__.py
│   │   └── authController.py
│   │   └── todoController.py
```

## 2. 모든 controller를 `main.py`에서 `controllers`로 이동시킨다.

AuthController — `/login`, `/users` (2)
TodoController — `/todos`, `/todos/{todo_id}` (4)

**app/controllers/AuthContoller.py**

```python
# app/controllers/AuthContoller.py

import json

from fastapi import Body, Response, status, HTTPException, Depends
from sqlalchemy.exc import NoResultFound

from auth.token import create_access_token
from deps import get_user_service
from exceptions.EmailDuplicationException import EmailDuplicationException
from exceptions.InvalidData import InvalidData
from exceptions.UserNotFoundException import UserNotFoundException
from exceptions.UsernameDuplicationException import UsernameDuplicationException
from requests.CreateUserRequest import CreateUserRequest
from requests.LoginRequest import LoginRequest

from services.UserService import UserService


@app.post("/login")
async def login(*, user_service: UserService = Depends(get_user_service),
                request: LoginRequest = Body()):
    try:
        user = await user_service.login(request)

        access_token = create_access_token(
            data={"sub": user.username},
        )

        return Response(status_code=status.HTTP_200_OK,
                        content=json.dumps({"access_token": access_token, "token_type": "bearer"}))
    except UserNotFoundException as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=str(e),
            headers={"WWW-Authenticate": "Bearer"},
        )
    except NoResultFound:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid password or username")
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail={
            "message": "Something went wrong",
            "code": "INTERNAL_SERVER_ERROR",
            "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
        })


@app.post("/users", response_class=Response, status_code=status.HTTP_201_CREATED)
async def create_user(*, user_service: UserService = Depends(get_user_service),
                      request: CreateUserRequest = Body()):
    try:
        await user_service.create_user(request)
        return Response(status_code=status.HTTP_201_CREATED)
    except InvalidData as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail={
            "message": str(e),
            "code": "INVALID_DATA",
            "status_code": status.HTTP_400_BAD_REQUEST,
        })
    except EmailDuplicationException as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail={
            "message": str(e),
            "code": "EMAIL_DUPLICATION",
            "status_code": status.HTTP_409_CONFLICT,
        })
    except UsernameDuplicationException as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail={
            "message": str(e),
            "code": "USERNAME_DUPLICATION",
            "status_code": status.HTTP_409_CONFLICT,
        })
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail={
            "message": "Something went wrong",
            "code": "INTERNAL_SERVER_ERROR",
            "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
        })
```

**app/controllers/todoController.py**

```python
# app/controllers/todoController.py

from fastapi import Body, Response, status, HTTPException, Depends, Query
from typing import Annotated
from sqlalchemy.exc import NoResultFound

from auth.user import get_current_user
from deps import get_todo_service
from models.Todo import Todo
from requests.CreateTodoRequest import CreateTodoRequest
from requests.UpdateTodoRequest import UpdateTodoRequest
from responses.GetByUsernameResponse import GetByUsernameResponse
from services.TodoService import TodoService


@app.get("/todos", response_model=list[Todo])
async def get_todos(*,
                    todo_service: TodoService = Depends(get_todo_service),
                    offset: Annotated[int | None, Query()] = 0,
                    limit: Annotated[int | None, Query()] = 10,
                    current_user: GetByUsernameResponse = Depends(get_current_user),
                    ):
    print(current_user) # @TODO We will use this later

    todo_list = await todo_service.get_todos(offset, limit)

    return todo_list


@app.get("/todos/{todo_id}", response_model=Todo)
async def get_todo(*,
                   todo_service: TodoService = Depends(get_todo_service),
                   todo_id: int
                   ):
    try:
        return await todo_service.get_todo(todo_id)
    except NoResultFound as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail={
                                "message": f"Todo [{todo_id}] not found",
                                "status_code": status.HTTP_404_NOT_FOUND,
                                "code": "TODO_NOT_FOUND",
                            })
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail={
                                "message": "Something went wrong",
                                "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
                                "code": "INTERNAL_SERVER_ERROR",
                            })


@app.post("/todos", response_model=Todo)
async def create_todo(*, data: CreateTodoRequest = Body(), todo_service: TodoService = Depends(get_todo_service)):
    new_todo = await todo_service.create_todo(data)

    return new_todo


@app.patch("/todos", status_code=status.HTTP_204_NO_CONTENT)
async def update_todo(*, todo: UpdateTodoRequest = Body(),
                      todo_service: TodoService = Depends(get_todo_service), ):
    await todo_service.update_todo(todo)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@app.delete("/todos/{todo_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_todo(*, todo_id: int, todo_service: TodoService = Depends(get_todo_service)):
    await todo_service.delete_todo(todo_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
```

하지만... 잠깐만, 안 되네요. 맞아요. FastAPI에 우리 그룹이 무엇인지 말하고 등록해야 한다.

![](./images/term_shot_09.png)

## 3. ApitRoutes

아래 코드를 `authController.py`에 추가한다.

```python
auth_router = APIRouter(
    prefix="/auth",
    tags=["Users"]
)

# from fastapi import Body, Response, status, HTTPException, Depends, APIRouter
```

아래 코드를 `todoController.py`에 추가한다.

```python
todo_router = APIRouter(
    prefix="/todos",
    tags=["Todos"]
)

# from fastapi import Body, Response, status, HTTPException, Depends, Query, APIRouter
```

지금은 태그를 무시해도 된다. 스웨거 정의에 유용하다.

모든 앱 데코레이터를 `todo_router` 또는 `auth_router`로 변경한다.

```python
@todo.get("/todos", response_model=list[Todo])
async def get_todos(*,
                    todo_service: TodoService = Depends(get_todo_service),
                    offset: Annotated[int | None, Query()] = 0,
                    limit: Annotated[int | None, Query()] = 10,
                    current_user: GetByUsernameResponse = Depends(get_current_user),
                    ):
    print(current_user) # @TODO We will use this later

    todo_list = await todo_service.get_todos(offset, limit)

    return todo_list
```

## 4. `main.py`의 api 경로 등록

```python
# app/main.py

from fastapi import FastAPI

from controllers.AuthController import auth_router
from controllers.TodoController import todo_router
from database import init_db


app = FastAPI()


@app.on_event("startup")
async def on_startup():
    await init_db()

app.include_router(auth_router, prefix="/api/v1")
app.include_router(todo_router, prefix="/api/v1")
```

login 엔드포인트를 호출하려면 `/login`을 사용할 수 없다. 대신 main 파일에 등록한 적절한 버전을 찾아서 APIRouter에서 해당 정의를 확인해야 한다.



```python
auth_router = APIRouter(
    prefix="/api/vi/auth",
    tags=["Users"]
)

...

@auth_router.post("/api/vi/auth/login")
async def login(*, user_service: UserService = Depends(get_user_service),
                request: LoginRequest = Body()):
    try:
        user = await user_service.login(request)

        access_token = create_access_token(
            data={"sub": user.username},
        )
...
```

### Login 테스트

> login 엔드포인트를 테스트해 보자. 이전 파트의 user를 사용하겠다.

![](./images/term_shot_10.webp)

### todo 생성 테스트

![](./images/term_shot_11.webp)

### 모든 todo 검색 테스트

![](./images/term_shot_12.webp)


<a name="footnote_1">1</a>: 이 페이지는
[FastAPI todo list with authentication — ApiRouter (5/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-4-6e8dc3a07997)를 편역한 것임.
