# CRUD <sup>[1](#footnote_1)</sup>

[지난 포스팅](./database.md)에서는 todo 생성 기능을 추가했다. CRUD를 마무리하고 **DELETE**, **UPDATE**, **GET ALL** 및 **GET ONE TODO**를 추가해 보겠다.

> 이 포스팅은 [이전 포스팅](./database.md)의 후속이다. 아직 읽어보지 않았다면 더 나은 맥락과 이해를 위해 읽어보시기를 적극 권장한다.

여기서는 한 가지 접근 방식을 소개하지만, 이러한 작업을 구현하는 데 사용할 수 있는 여러 가지 옵션이 있다는 점에 유의하세요.

각 단계에서 서비스를 구현하고 기존 컨트롤러를 새 컨트롤러로 교체하겠다.

이것이 데이터베이스의 현재 상태이다.

![](./images/db_state_01.webp)

> **Note**: <br>
> 위의 그림은 단순히 현재 데이터베이스 상태를 보이기 위한 것이다.

시작하기 전에 요청 폴더에 이 요청 정의를 생성한다.

```python
# app/requests/updateTodoRequest.py

from pydantic import BaseModel


class UpdateTodoRequest(BaseModel):
    id: int
    value: str
```

```
...

│   ├── models
│   │   └── __init__.py
│   │   └── todo.py
│   ├── requests
│   │   └── __init__.py
│   │   └── createTodoRequest.py
│   │   └── updateTodoRequest.py
...
```

## 1. id로 한 todo 검색

### Service

`scalars().all()[0]` 메서드는 하나의 결과 또는 예외를 받도록 보장한다는 점을 기억하자. 따라서 결과를 얻는지 여부는 우리가 신경 쓸 일이 아니다. controller에서 이 예외를 처리할 것이다.

```python
# app/services/get_todo.py

from models.todo import Todo

async def get_todo(self, todo_id: int) -> Todo:
    query = (
        select(Todo)
        .where(Todo.id == todo_id)
    )
  
    result = await self.session.execute(query)
    return result.scalars().all()[0]
```

### Controller
`response_model`은 응답의 정의를 정의한다. 예를 들어, `Todo` 모델에 정의된 것보다 많은 데이터를 보내려고 하면 응답에서 해당 데이터가 반환되지 않는다.

```python
# Don't include this code
return [{"id": item.id, "value": item.value, "test": 1} for item in todo_list]

# You will get

[
    {
        "id": 1,
        "value": "Walk a dog"
    }
]
```

```python
# app/main.py

# ... import

from sqlalchemy.orm.exc import NoResultFound

# ... code

@app.get("/todos/{todo_id}", response_model=Todo)
async def get_todo(*,
                   todo_service: TodoService = Depends(get_todo_service),
                   todo_id: int
                   ):
    print(f"main>todo_id: {todo_id}")
    try:
        return await todo_service.get_todo(todo_id)
    except NoResultFound as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail={
                                "message": f"Todo [{todo_id}] not found",
                                "status_code": status.HTTP_404_NOT_FOUND,
                                "code": "TODO_NOT_FOUND",
                            })
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail={
                                "message": "Something went wrong",
                                "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
                                "code": "INTERNAL_SERVER_ERROR",
                            })
```

두 가지 예외가 있다. 첫 번째 예외는 예외를 발생시킬 수 있는 `.one()` 메서드와 관련된 것이다. 이 경우 404 - not found 상태 코드가 형식화된 오류 메시지와 함께 반환된다.

이 `message`는 주로 프로그래머를 위한 것이다.

이 `code`는 잠재적인 프런트엔드를 위한 번역용 이다.

`status_code`는 명확성을 위한 것이다. 반드시 이 방식을 따라야 하는 것은 아니다. 특정 요구 사항에 따라 오류 메시지를 자유롭게 수정할 수 있다.

두 번째 예외는 예기치 않은 시나리오나 문제가 발생했을 때를 위한 대체 방법이다. 버그는 개발 과정에서 피할 수 없는 부분이기 때문이다.

**[GET] localhost:8000/todos/1**

![](./images/terminal_shot_01.webp)

## 2. 페이지네이션을 사용하여 모든 todo 가져오기

### Service

```python
async def get_todos(self, offset: int, limit: int) -> list[Todo]:
  query = (
      select(Todo)
      .offset(offset)
      .limit(limit)
  )
  
  data = await self.session.execute(query)
  return data.scalars().all()
```

### Controller

```python
@app.get("/todos", response_model=list[Todo])
async def get_todos(*,
                    todo_service: TodoService = Depends(get_todo_service),
                    offset: Annotated[int | None, Query()] = 0,
                    limit: Annotated[int | None, Query()] = 10,
                    ):
    todo_list = await todo_service.get_todos(offset, limit)

    return todo_list
```

> FastAPI 버전 0.95.1 이상이 설치되어 있어야 한다.

함수 매개변수의 기본값 대신 **`Annotated`**를 사용하는 것을 권한다.

자세한 내용은 [여기](https://fastapi.tiangolo.com/tutorial/query-params-str-validations/#advantages-of-annotated)에서 확인할 수 있다.

[GET] localhost:8000/todos

![](./images/terminal_shot_02.webp)

할 일을 더 추가하고 페이지 매김이 제대로 작동하는지 테스트하여야 한다. 또한 페이지네이션을 위한 쿼리 매개변수를 직접 설정해 보세요.

## 3. todo 삭제
todo를 삭제하는 방법은 간단하다. todo가 존재하지 않는다면 아무런 조치를 취할 필요가 없다. todo의 존재 여부를 확인하는 버전을 구현하도록 선택할 수 있다. 현재로서는 괜찮다.

### Service

```python
async def delete_todo(self, todo_id: int) -> None:
  query = (
      delete(Todo)
      .where(Todo.id == todo_id)
  )
  
  await self.session.execute(query)
  await self.session.commit()
```

### Controller

```python
@app.delete("/todos/{todo_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_todo(*, todo_id: int, todo_service: TodoService = Depends(get_todo_service)):
    await todo_service.delete_todo(todo_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
```

**[DELETE] localhost:8000/todos/1**

![](./images/terminal_shot_03.webp)

![](./images/db_state_02.webp)

## 4. todo 업데이트

### Service

```python
async def update_todo(self, new_todo: UpdateTodoRequest) -> None:
  query = (
      update(Todo)
      .values(value=new_todo.value)
      .where(Todo.id == new_todo.id)
  )
  
  await self.session.execute(query)
  await self.session.commit()
```

### Controller

```python
@app.patch("/todos", status_code=status.HTTP_204_NO_CONTENT)
async def update_todo(*, todo: UpdateTodoRequest = Body(),
                      todo_service: TodoService = Depends(get_todo_service), ):
    await todo_service.update_todo(todo)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
```

**[PATCH] localhost:8000/todos**

![](./images/terminal_shot_04.webp)

![](./images/db_state_03.webp)

## 5. 최종 결과

**app/main.py**

```python
# app/main.py

from fastapi import FastAPI, Body, Response, status, HTTPException, Depends, Query
from typing import Optional, Annotated
from pydantic import BaseModel
from sqlalchemy.exc import NoResultFound

from database import init_db
from deps import get_todo_service
from requests.CreateTodoRequest import CreateTodoRequest
from requests.UpdateTodoRequest import UpdateTodoRequest
from services.TodoService import TodoService

app = FastAPI()


@app.on_event("startup")
async def on_startup():
    await init_db()


class Todo(BaseModel):
    id: Optional[int]
    value: str


@app.get("/todos", response_model=list[Todo])
async def get_todos(*,
                    todo_service: TodoService = Depends(get_todo_service),
                    offset: Annotated[int | None, Query()] = 0,
                    limit: Annotated[int | None, Query()] = 10,
                    ):
    todo_list = await todo_service.get_todos(offset, limit)

    return todo_list


@app.get("/todos/{todo_id}", response_model=Todo)
async def get_todo(*,
                   todo_service: TodoService = Depends(get_todo_service),
                   todo_id: int
                   ):
    try:
        return await todo_service.get_todo(todo_id)
    except NoResultFound as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail={
                                "message": f"Todo [{todo_id}] not found",
                                "status_code": status.HTTP_404_NOT_FOUND,
                                "code": "TODO_NOT_FOUND",
                            })
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail={
                                "message": "Something went wrong",
                                "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
                                "code": "INTERNAL_SERVER_ERROR",
                            })


@app.post("/todos", response_model=Todo)
async def create_todo(*, data: CreateTodoRequest = Body(), todo_service: TodoService = Depends(get_todo_service)):
    new_todo = await todo_service.create_todo(data)

    return new_todo


@app.patch("/", status_code=status.HTTP_204_NO_CONTENT)
async def update_todo(*, todo: UpdateTodoRequest = Body(),
                      todo_service: TodoService = Depends(get_todo_service), ):
    await todo_service.update_todo(todo)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@app.delete("/todos/{todo_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_todo(*, todo_id: int, todo_service: TodoService = Depends(get_todo_service)):
    await todo_service.delete_todo(todo_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
```

**todoService.py**

```python
# app/services/todoService.py

from sqlmodel import Session, select, delete, update

from models.todo import Todo
from requests.createTodoRequest import CreateTodoRequest
from requests.updateTodoRequest import UpdateTodoRequest


class TodoService:
    def __init__(self, session: Session):
        self.session = session

    async def create_todo(self, data: CreateTodoRequest):
        new_todo = Todo(value=data.value)

        self.session.add(new_todo)
        await self.session.commit()

        return new_todo

    async def get_todo(self, todo_id: int) -> Todo:
        # print(f"todoService>get_todo.todo_id: {todo_id}")
        query = select(Todo).where(Todo.id == todo_id)
        
        result = await self.session.execute(query)
        return result.scalars().all()[0]
    
    async def get_todos(self, offset: int, limit: int) -> list[Todo]:
        query = (
            select(Todo)
            .offset(offset)
            .limit(limit)
        )
        
        data = await self.session.execute(query)
        return data.scalars().all()
    
    async def update_todo(self, new_todo: UpdateTodoRequest) -> None:
        query = (
            update(Todo)
            .values(value=new_todo.value)
            .where(Todo.id == new_todo.id)
        )
        print(f"todoService>query: {query}")

        await self.session.execute(query)
        await self.session.commit()

    async def delete_todo(self, todo_id: int) -> None:
        query = (
            delete(Todo)
            .where(Todo.id == todo_id)
        )

        await self.session.execute(query)
        await self.session.commit()
```


<a name="footnote_1">1</a>: 이 페이지는
[FastAPI todo list with authentication — CRUD (3/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-3-ce29a66fea4d)를 편역한 것임.
