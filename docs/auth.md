# 인증 <sup>[1](#footnote_1)</sup>

이제 우리의 경로를 강화할 때이다! 하지만 우리의 입지를 다지기 전에 강력한 패키지로 무장하고 기반을 다져야 한다. 첫 번째 임무는? 강력한 로그인과 등록 기능 활용하는 것이다.

> 이 글은 [이전 글](./crud.md)의 후속 글이다. 아직 읽어보지 않았다면 더 나은 맥락과 이해를 위해 읽기를 적극 권장한다.

사용자와 사용자 비밀번호 모델을 추가하는 것부터 시작하겠다. 이것이 없으면 현재 사용자가 누구인지 알 수 없다.

> 길을 잃었을 때를 대비해 하단에 최종 폴더 구조를 추가했다.

**user.py**

```python
# models/user.py

import datetime

from sqlmodel import SQLModel, Field, Relationship

import sqlalchemy as sa


class User(SQLModel, table=True):
    __tablename__ = "users"

    user_id: int = Field(default=None, primary_key=True)
    username: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False, unique=True))
    first_name: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False))
    last_name: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False))
    email: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False, unique=True))
    birthdate: datetime.date = Field(nullable=False)
    created_at: str = Field(sa_column=sa.Column(sa.DateTime(timezone=True), default=datetime.datetime.now))

    passwords: list["UserPassword"] = Relationship(back_populates="user", sa_relationship_kwargs={'lazy': 'joined'})
```

**userPassord.py**

```python
# models/userPassword.py

import datetime

from sqlmodel import SQLModel, Field, Relationship

import sqlalchemy as sa

from models.User import User


class UserPassword(SQLModel, table=True):
    __tablename__ = "user_passwords"

    user_password_id: int = Field(default=None, primary_key=True)
    value: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False))
    user_id: int = Field(
        sa_column=sa.Column(sa.Integer, sa.ForeignKey(User.user_id, ondelete="CASCADE"), nullable=False))
    created_at: datetime.datetime = Field(sa_column=sa.Column(sa.DateTime(timezone=True), default=datetime.datetime.now))
    updated_at: datetime.datetime = Field(sa_column=sa.Column(sa.DateTime(timezone=True), default=datetime.datetime.now))

    user: User = Relationship(back_populates="passwords")
```

## 2 패키지 설치

```bash
(venv) $ pip install "python-jose[cryptography]"
(venv) $ pip install "passlib[bcrypt]"
```

비밀번호를 일반 텍스트로 저장하고 싶지 않다. 데이터베이스를 도난당하면 해커가 사용자의 비밀번호를 읽을 수 없는데, 이는 비밀번호가 해시화되어 있기 때문이다.

`PassLib`은 비밀번호 해시를 처리한다.

`Python-jose`는 JWT 토큰을 생성하고 확인한다.

UserService를 만드는 것부터 시작하겠다. 아직 완전한 기능을 구현하지는 않겠지만 이 단계는 JWT와 비밀번호 설정을 마무리하는 데 필수적이다.

**userService.py**

```python
# services/userService.py

from sqlmodel import Session


class UserService:
    def __init__(self, session: Session):
        self.session = session
```

**deps.py [UPDATED]**

```python
# app/deps.py

from database import async_session
from services.UserService import UserService

# code ...

async def get_user_service():
    async with async_session() as session:
        async with session.begin():
            yield UserService(session)
```

루트에서 `auth` 폴더를 생성하고 여기에 `password.py`, `token.py` 및 `user.py` 세 파일을 만든다.

**auth/password.py**

```python
# auth/password.py

from passlib.context import CryptContext


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)
```

이 파일은 비밀번호를 해싱하고 데이터베이스의 비밀번호가 요청에서의 비밀번호와 동일한지 확인하는 작업을 처리한다.

**token.py**

```python
# auth/token.py

from dotenv import dotenv_values
from pydantic import BaseModel
from jose import jwt
from datetime import datetime, timedelta

config = dotenv_values("./.env")

SECRET_KEY = config.get("SECRET_KEY")
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str


def create_access_token(data: dict):
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    data.update({"exp": expire})
    return jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)
```

이 파일은 JWT 토큰을 설정한다. `create_access_token` 함수를 사용하여 유효 기간이 30분인 JWT 토큰을 생성할 수 있다. 그 후에는 새 토큰을 받아야 한다.

**auth/user.py**

```python
# auth/user.py

from pydantic import BaseModel
from fastapi import Depends, status, HTTPException
from jose import JWTError, jwt
from fastapi.security import OAuth2PasswordBearer

from deps import get_user_service
from services.UserService import UserService
from user.token import SECRET_KEY, ALGORITHM

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


class TokenData(BaseModel):
    username: str | None = None


async def get_current_user(
        token: str = Depends(oauth2_scheme),
        user_service: UserService = Depends(get_user_service)
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception

    user = await user_service.get_by_username(token_data.username) # not implemented yet
    if user is None:
        raise credentials_exception

    return user
```

이 파일은 매우 중요하다. 이 파일은 요청에서 JWT 토큰을 파싱하려고 시도한다. 토큰이 없거나 사용자가 데이터베이스에 존재하지 않는 경우 `credentials_exception`을 생성허여야 한다.

> `.env`에 `SECRET_KEY`를 추가하는 것을 잊지 마세요.
>
> SECRET_KEY=09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7

요청이 어떻게 표시될지 정의해야 한다. 또한 커스텀 예외를 정의하고 싶다. 데이터베이스에서 사용자 이름과 이메일 필드를 고유하게 설정했다. 따라서 사용자가 데이터베이스에 이미 존재하는 사용자 이름이나 이메일을 추가하려고 하는 경우에 대비하여 예외를 커스터마이징해야 한다. `InvalidDataException`은 요청에 유효하지 않은 데이터가 포함된 경우를 위한 것이다.

**requests/loginRequest.py**

```python
# requests/loginRequest.py

from pydantic import BaseModel


class LoginRequest(BaseModel):
    username: str
    password: str
```

**request/createUserRequest.py**

```python
# request/createUserRequest.py

from pydantic import BaseModel


class CreateUserRequest(BaseModel):
    first_name: str
    last_name: str
    email: str
    birthdate: str
    username: str
    password: str
```

**exceptions/userNotFoundException.py**

```python
# exceptions/UserNotFoundException.py

class UserNotFoundException(Exception):
    pass
```

**exceptions/usernameDuplicationException.py**

```python
# exceptions/usernameDuplicationException.py

class UsernameDuplicationException(Exception):
    pass
```

**exceptions/emailDuplicationException.py**

```python
# exceptions/emailDuplicationException.py

class EmailDuplicationException(Exception):
    pass
```

**exceptions/invalidData.py**

```python
# exceptions/invalidData.py

class InvalidDataException(Exception):
    pass
```

`UserService`는 매우 중요한 파일입니다. 이 파일은 데이터베이스와 통신한다.

```python
# service/userService.py

import asyncpg
from sqlmodel import Session, select
from sqlalchemy import exc

from auth.password import get_password_hash, verify_password
from exceptions.EmailDuplicationException import EmailDuplicationException
from exceptions.UserNotFoundException import UserNotFoundException
from models.User import User
from models.UserPassword import UserPassword
from requests.CreateUserRequest import CreateUserRequest
from requests.LoginRequest import LoginRequest


class UserService:
    def __init__(self, session: Session):
        self.session = session

    async def create_user(self, data: CreateUserRequest):
        try:
            new_user = User(
                first_name=data.first_name,
                last_name=data.last_name,
                email=data.email,
                birthdate=data.birthdate,
                username=data.username,
                passwords=[UserPassword(value=get_password_hash(data.password))],
            )

            self.session.add(new_user)
            await self.session.commit()

            return new_user
        except (exc.IntegrityError, asyncpg.exceptions.UniqueViolationError):
            raise EmailDuplicationException(f"Email [{data.email}] already exists")
        except Exception as e:
            raise Exception(e)

    async def get_by_username(self, username: str):
        query = (
            select(User.user_id, User.username, User.email, UserPassword.value.label("password"))
            .join(UserPassword)
            .where(User.username == username)
            .limit(1)
        )

        result = await self.session.execute(query)
        return result.first()

    async def login(self, data: LoginRequest):
        user = await self.get_by_username(data.username)

        if not user:
            raise UserNotFoundException("Username or password is invalid")

        if not verify_password(data.password, user.password):
            raise UserNotFoundException("Username or password is invalid")

        return user
```

완전히 작동하려면 로그인과 사용자 등록을 위한 컨트롤러를 설정해야 한다.

**app/main.py[Login]**

```python
@app.post("/login")
async def login(*, user_service: UserService = Depends(get_user_service),
                request: LoginRequest = Body()):
    try:
        user = await user_service.login(request)

        access_token = create_access_token(
            data={"sub": user.username}
        )

        return Response(status_code=status.HTTP_200_OK,
                        content=json.dumps({"access_token": access_token, "token_type": "bearer"}))
    except UserNotFoundException as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=str(e),
            headers={"WWW-Authenticate": "Bearer"},
        )
    except NoResultFound:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid password or username")
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail={
            "message": "Something went wrong",
            "code": "INTERNAL_SERVER_ERROR",
            "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
        })
```

**app/main.py[Register]

```python
@app.post("/users", response_class=Response, status_code=status.HTTP_201_CREATED)
async def create_user(*, user_service: UserService = Depends(get_user_service),
                      request: CreateUserRequest = Body()):
    try:
        await user_service.create_user(request)
        return Response(status_code=status.HTTP_201_CREATED)
    except EmailDuplicationException as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail={
            "message": str(e),
            "code": "EMAIL_DUPLICATION",
            "status_code": status.HTTP_409_CONFLICT,
        })
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail={
            "message": "Something went wrong",
            "code": "INTERNAL_SERVER_ERROR",
            "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
        })
```

**responses/getByUsernameResponse.py**

```python
# responses/getByUsernameResponse.py

from pydantic import BaseModel


class GetByUsernameResponse(BaseModel):
    user_id: str
    username: str
    email: str
    password: str
```

경로를 보호하려면 한 줄을 추가해야 한다. 

**get_todos**

```python
@app.get("/todos", response_model=list[Todo])
async def get_todos(*,
                    todo_service: TodoService = Depends(get_todo_service),
                    offset: Annotated[int | None, Query()] = 0,
                    limit: Annotated[int | None, Query()] = 10,
                    current_user: GetByUsernameResponse = Depends(get_current_user),
                    ):
    print(current_user) # @TODO We will use this later 
    
    todo_list = await todo_service.get_todos(offset, limit)

    return todo_list
```

이 줄은 경로를 보호하는 역할을 한다. 매우 간단하다. 예외를 발생시키거나 JWT 토큰에서 현재 사용자를 반환한다.

```python
current_user: GetByUsernameResponse = Depends(get_current_user)
```

## `main.py` 완성

```python
# app/main.py

mport json

from fastapi import FastAPI, Body, Response, status, HTTPException, Depends, Query
from typing import Optional, Annotated
from pydantic import BaseModel
from sqlalchemy.exc import NoResultFound

from auth.token import create_access_token
from auth.user import get_current_user
from database import init_db
from deps import get_todo_service, get_user_service
from exceptions.EmailDuplicationException import EmailDuplicationException
from exceptions.InvalidData import InvalidData
from exceptions.UserNotFoundException import UserNotFoundException
from exceptions.UsernameDuplicationException import UsernameDuplicationException
from requests.CreateTodoRequest import CreateTodoRequest
from requests.CreateUserRequest import CreateUserRequest
from requests.LoginRequest import LoginRequest
from requests.UpdateTodoRequest import UpdateTodoRequest
from responses.GetByUsernameResponse import GetByUsernameResponse
from services.TodoService import TodoService
from services.UserService import UserService

app = FastAPI()


@app.on_event("startup")
async def on_startup():
    await init_db()


class Todo(BaseModel):
    id: Optional[int]
    value: str


@app.get("/todos", response_model=list[Todo])
async def get_todos(*,
                    todo_service: TodoService = Depends(get_todo_service),
                    offset: Annotated[int | None, Query()] = 0,
                    limit: Annotated[int | None, Query()] = 10,
                    current_user: GetByUsernameResponse = Depends(get_current_user),
                    ):
    print(current_user) # @TODO We will use this later

    todo_list = await todo_service.get_todos(offset, limit)

    return todo_list


@app.get("/todos/{todo_id}", response_model=Todo)
async def get_todo(*,
                   todo_service: TodoService = Depends(get_todo_service),
                   todo_id: int
                   ):
    try:
        return await todo_service.get_todo(todo_id)
    except NoResultFound as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail={
                                "message": f"Todo [{todo_id}] not found",
                                "status_code": status.HTTP_404_NOT_FOUND,
                                "code": "TODO_NOT_FOUND",
                            })
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail={
                                "message": "Something went wrong",
                                "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
                                "code": "INTERNAL_SERVER_ERROR",
                            })


@app.post("/todos", response_model=Todo)
async def create_todo(*, data: CreateTodoRequest = Body(), todo_service: TodoService = Depends(get_todo_service)):
    new_todo = await todo_service.create_todo(data)

    return new_todo


@app.patch("/todos", status_code=status.HTTP_204_NO_CONTENT)
async def update_todo(*, todo: UpdateTodoRequest = Body(),
                      todo_service: TodoService = Depends(get_todo_service), ):
    await todo_service.update_todo(todo)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@app.delete("/todos/{todo_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_todo(*, todo_id: int, todo_service: TodoService = Depends(get_todo_service)):
    await todo_service.delete_todo(todo_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@app.post("/users", response_class=Response, status_code=status.HTTP_201_CREATED)
async def create_user(*, user_service: UserService = Depends(get_user_service),
                      request: CreateUserRequest = Body()):
    try:
        await user_service.create_user(request)
        return Response(status_code=status.HTTP_201_CREATED)
    except InvalidData as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail={
            "message": str(e),
            "code": "INVALID_DATA",
            "status_code": status.HTTP_400_BAD_REQUEST,
        })
    except EmailDuplicationException as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail={
            "message": str(e),
            "code": "EMAIL_DUPLICATION",
            "status_code": status.HTTP_409_CONFLICT,
        })
    except UsernameDuplicationException as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail={
            "message": str(e),
            "code": "USERNAME_DUPLICATION",
            "status_code": status.HTTP_409_CONFLICT,
        })
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail={
            "message": "Something went wrong",
            "code": "INTERNAL_SERVER_ERROR",
            "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
        })


@app.post("/login")
async def login(*, user_service: UserService = Depends(get_user_service),
                request: LoginRequest = Body()):
    try:
        user = await user_service.login(request)

        access_token = create_access_token(
            data={"sub": user.username}
        )

        return Response(status_code=status.HTTP_200_OK,
                        content=json.dumps({"access_token": access_token, "token_type": "bearer"}))
    except UserNotFoundException as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=str(e),
            headers={"WWW-Authenticate": "Bearer"},
        )
    except NoResultFound:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid password or username")
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail={
            "message": "Something went wrong",
            "code": "INTERNAL_SERVER_ERROR",
            "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
        })
```

**[GET]http://localhost:8000/todos**

이 엔드포인트를 호출하려고 하면 이 줄 때문에 이 내용을 볼 수 있을 것이다.

```
this line current_user: GetByUsernameResponse = Depends(get_current_user)
```

![](./images/term_shot_05.webp)

### register 엔트포인트 테스트

![](./images/term_shot_06.webp)

### login 엔트포인트 테스트

![](./images/term_shot_07.webp)

JWT 토큰을 추가하면 경로를 사용할 수 있는지 테스트한다.

![](./images/term_shot_08.webp)

## 최종 프로젝트 디렉토리 구조

```
todo-list
├── .env
├── .git
├── .gitignore
├── LICENSE
├── README.md
├── app
│   ├── __init__.py
│   ├── auth
│   │   └── __init__.py
│   │   └── password.py
│   │   └── token.py
│   │   └── user.py
│   ├── exceptions
│   │   └── __init__.py
│   │   └── emailDuplicationException.py
│   │   └── invalidData.py
│   │   └── usernameDuplicationException.py
│   │   └── userNotFoundException.py
│   ├── database.py
│   ├── deps.py
│   ├── main.py
│   ├── models
│   │   └── __init__.py
│   │   └── todo.py
│   │   └── user.py
│   │   └── userPassword.py
│   ├── requests
│   │   └── __init__.py
│   │   └── createTodoRequest.py
│   │   └── createUserRequest.py
│   │   └── loginRequest.py
│   │   └── updateTodoRequest.py
│   ├── responses
│   │   └── __init__.py
│   │   └── getByUsernameResponse.py
│   ├── services
│   │   └── __init__.py
│   │   └── todoService.py
│   │   └── userService.py
│   └── tests
│       └── __init__.py
├── requirements.txt
└── venv
```

이렇게 하면 경로를 보호하고 JWT 토큰을 사용할 수 있다.


<a name="footnote_1">1</a>: 이 페이지는
[FastAPI todo list with authentication — Auth (4/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-4-6e8dc3a07997)를 편역한 것임.
