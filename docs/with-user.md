# 사용자와 todo <sup>[1](#footnote_1)</sup>

개발의 다음 단계는 사용자와 todo 연결하는 것이다.

이를 위해서는 사용자와 todo 간의 관계를 생성하도록 모델을 수정해야 한다. 또한 이 새로운 관계를 처리할 수 있도록 현재 서비스를 업데이트하고, 사용자가 todo를 만들고, 업데이트하고, 삭제할 수 있도록 새로운 서비스와 컨트롤러를 추가해야 한다.

> 이 포스팅은 [이전 포스팅](./auth.md)의 후속 글이다. 아직 읽어보지 않았다면 더 나은 맥락과 이해를 위해 읽기를 적극 권장한다.

사용자와 todo를 연결하기 위해 가장 먼저 해야 할 일은 모델을 업데이트하는 것이다. 현재 사용자 모델은 `Todo` 모델과 연결되어 있지 않다. 이 관계를 설정하려면 기존 모델을 몇 가지 수정해야 한다.

수정해야 할 사항 중 하나는 `Todo` 모델의 `value` 컬럼에서 고유 제약 조건을 제거하는 것이다.

다음으로, `Todo` 모델의 `value`와 `user_id` 컬럼에 복합 고유 제약 조건이라는 새 고유 제약 조건을 만든다. 이렇게 하면 각 사용자가 특정 값의 todo을 하나만 가질 수 있다.

**todo.py**

```python
# models/todo.py

import datetime
from typing import Optional

from sqlmodel import SQLModel, Field

import sqlalchemy as sa


class Todo(SQLModel, table=True):
    __tablename__ = "todos"

    id: Optional[int] = Field(default=None, primary_key=True)
    value: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False))
    user_id: int = Field(sa_column=sa.Column(sa.Integer, sa.ForeignKey(User.user_id, ondelete="CASCADE"), nullable=False))
    created_at: datetime.datetime = Field(sa_column=sa.Column(sa.DateTime(timezone=True), datetime.datetime.now))

    user: User = Relationship(back_populate="todos", sa_relationship.kwargs={'lazy': 'selection'})

    __table_args__ = (
        UniaueConstraint("value", "user_id", name="unique_todos_user_id_values"),
    )
```

**user.py**

```python
# models/user.py

import datetime

from sqlmodel import SQLModel, Field, Relationship

import sqlalchemy as sa


class User(SQLModel, table=True):
    __tablename__ = "users"

    user_id: int = Field(default=None, primary_key=True)
    username: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False, unique=True))
    first_name: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False))
    last_name: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False))
    email: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False, unique=True))
    birthdate: datetime.date = Field(nullable=False)
    created_at: str = Field(sa_column=sa.Column(sa.DateTime(timezone=True), default=datetime.datetime.now))

    passwords: list["UserPassword"] = Relationship(back_populates="user", sa_relationship_kwargs={'lazy': 'joined'})
    todos: list["Todo"] = Relationship(back_populates="user", sa_relationship_kwargs={'lazy': 'selectin'})
```

이제 사용자와 todo를 연결하도록 모델을 업데이트했으므로 다음 단계는 `userService.py`에서 `create_todo` 메서드를 제거하는 것이다. 
사용자와 연결하지 않고 todo를 만드는 것은 의미가 없기 때문이다.

```python
async def create_todo(self, data: CreateTodoRequest):
        new_todo = Todo(value=data.value)

        self.session.add(new_todo)
        await self.session.commit()

        return new_todo
```

...와 `todoController.py`의 관련 엔드포인트

```python
@todo_router.post("/", response_model=Todo)
async def create_todo(*, data: CreateTodoRequest = Body(), todo_service: TodoService = Depends(get_todo_service)):
    new_todo = await todo_service.create_todo(data)

    return new_todo
```

사용자와 todo를 연결하고 사용자 todo를 생성하고, 관리할 수 있도록 `userTodoService.py`라는 새 서비스를 생성한다.

이 서비스는 사용자 todo의 CRUD 작업을 처리한다.

**userTodoService.py**

```python
# app/services/userTodoService.py

import asyncpg
from sqlalchemy import exc
from sqlalchemy.cimmutabledict import immutabledict
from sqlmodel import select, and_, delete, update

from requests import Session

from app.exceptions.TodoDuplicationException import TodoDuplicationException
from app.models.Todo import Todo
from app.models.User import User
from app.requests.CreateTodoRequest import CreateTodoRequest
from app.requests.UpdateTodoRequest import UpdateTodoRequest


class UserTodoService:
    def __init__(self, session: Session):
        self.session = session

    async def create_todo(self, data: CreateTodoRequest, user_id: str):
        try:
            new_todo = Todo(value=data.value, user_id=user_id)

            self.session.add(new_todo)
            await self.session.commit()

            return new_todo
        except (exc.IntegrityError, asyncpg.exceptions.UniqueViolationError):
            raise TodoDuplicationException(f"Todo [{data.value}] already exists")
        except Exception as e:
            raise Exception(e)

    async def get_todos(self, user_id: int, offset: int, limit: int):
        query = (
            select(Todo)
            .where(Todo.user_id == user_id)
            .offset(offset)
            .limit(limit)
        )

        result = await self.session.execute(query)
        return result.scalars().all()

    async def get_todo(self, user_id: int, todo_id: int):
        query = (
            select(Todo)
            .where(and_(Todo.user_id == user_id, Todo.id == todo_id))
        )

        result = await self.session.execute(query)
        return result.scalars().one()

    async def delete_todo(self, user_id: int, todo_id: int) -> None:
        query = (
            delete(Todo)
            .where(and_(Todo.id == todo_id, User.user_id == user_id))
        )

        await self.session.execute(query, execution_options=immutabledict({"synchronize_session": 'fetch'}))
        await self.session.commit()

    async def update_todo(self, user_id: int, new_todo: UpdateTodoRequest) -> None:
        query = (
            update(Todo)
            .values(value=new_todo.value)
            .where(and_(Todo.id == new_todo.id, User.user_id == user_id))
        )

        await self.session.execute(query, execution_options=immutabledict({"synchronize_session": 'fetch'}))
        await self.session.commit()
```

새로운 클래스인 `TodoDuplicationException`이 있다.

이 예외는 사용자가 자신의 계정에 이미 존재하는 값으로 todo를 만들려고 하는 경우를 처리하는 데 사용된다.

**todoDuplicationException.py**

```python
# app/exceptions/todoDuplicationException.py

class TodoDuplicationException(Exception):
    pass
```

> 컨트롤러가 서비스를 사용할 수 있도록 `deps.py`에 서비스를 등록하는 것을 잊지 마세요.

```python
# app/deps.py

from app.services.UserTodoService import UserTodoService
from database import async_session
from app.services.TodoService import todoService
from app.services.UserService import userService


# ... 


async def get_user_todo_service():
    async with async_session() as session:
        async with session.begin():
            yield userTodoService(session)
```

**app/controllers/userTodoController.py**

```python
# app/controllers/userTodoController.py

from typing import Annotated

from fastapi import APIRouter, status, Body, Depends, HTTPException, Response, Query
from fastapi.responses import JSONResponse

from fastapi.encoders import jsonable_encoder
from app.auth.user import get_current_user
from app.deps import get_user_todo_service
from app.exceptions.TodoDuplicationException import TodoDuplicationException
from app.responses.GetByUsernameResponse import GetByUsernameResponse
from app.services.UserTodoService import UserTodoService
from app.requests.CreateTodoRequest import CreateTodoRequest
from app.requests.UpdateTodoRequest import UpdateTodoRequest

user_todo_router = APIRouter(
    prefix="/users/todos",
    tags=["User todos"]
)


@user_todo_router.post("/", status_code=status.HTTP_201_CREATED)
async def create_todo(*, todo: CreateTodoRequest = Body(),
                      todo_service: UserTodoService = Depends(get_user_todo_service),
                      current_user: GetByUsernameResponse = Depends(get_current_user),
                      ):
    try:
        new_todo = await todo_service.create_todo(todo, current_user.user_id)
        created_at = new_todo.created_at
        todo_id = new_todo.id
        value = new_todo.value

        return JSONResponse(status_code=status.HTTP_201_CREATED,
                            content=jsonable_encoder(
                                {
                                    "todo_id": todo_id,
                                    "value": value,
                                    "created_at": created_at,
                                }
                            ))
    except TodoDuplicationException as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail={
            "message": str(e),
            "code": "EMAIL_DUPLICATION",
            "status_code": status.HTTP_409_CONFLICT,
        })
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail={
            "message": "Something went wrong",
            "code": "INTERNAL_SERVER_ERROR",
            "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
        })


@user_todo_router.get("/")
async def get_todos(*,
                    todo_service: UserTodoService = Depends(get_user_todo_service),
                    offset: Annotated[int | None, Query()] = 0,
                    limit: Annotated[int | None, Query()] = 10,
                    current_user: GetByUsernameResponse = Depends(get_current_user),
                    ):
    todo = await todo_service.get_todos(current_user.user_id, offset, limit)

    return todo


@user_todo_router.get("/{todo_id}")
async def get_todo(*,
                   todo_id: int,
                   todo_service: UserTodoService = Depends(get_user_todo_service),
                   current_user: GetByUsernameResponse = Depends(get_current_user),
                   ):
    todo = await todo_service.get_todo(current_user.user_id, todo_id)

    return todo


@user_todo_router.patch("/", status_code=status.HTTP_204_NO_CONTENT)
async def update_todo(*,
                      todo: UpdateTodoRequest = Body(),
                      todo_service: UserTodoService = Depends(get_user_todo_service),
                      current_user: GetByUsernameResponse = Depends(get_current_user),
                      ):
    await todo_service.update_todo(current_user.user_id, todo)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@user_todo_router.delete("/{todo_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_todo(*,
                      todo_id: int,
                      todo_service: UserTodoService = Depends(get_user_todo_service),
                      current_user: GetByUsernameResponse = Depends(get_current_user),
                      ):
    await todo_service.delete_todo(current_user.user_id, todo_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
```

... 그리고 `main.py`에 등록한다.

```python
# app/main.py

from fastapi import FastAPI, APIRouter

from app.controllers.AuthController import auth_router
from app.controllers.TodoController import todo_router
from app.controllers.UserTodoController import user_todo_router
from database import init_db

# ...

app.include_router(user_todo_router, prefix="/api/v1")
```

### todo 생성

> 무기명 토큰을 추가하는 것을 잊지 마세요.

![](./images/term_shot_13.webp)

### 모든 todo 읽기

> 작동 여부를 확인하기 위해 두 개 이상의 todo를 만들었다.

![](./images/term_shot_14.webp)

### todo 업데이트

![](./images/term_shot_15.webp)

### todo 읽기

![](./images/term_shot_16.webp)

### todo 삭제

![](./images/term_shot_17.webp)

이 시리즈는 여기까지이다. 수고하셨습니다! 도움이 되셨기를 바랍니다. REST API를 개선하는 데 도움이 되는 세 개의 보너스 글을 공유한다.

- API를 더 쉽게 이해하고 사용할 수 있도록 Swagger 설명서를 추가한다.
- REST API 배포하기
- 예외를 적절히 처리하여 API가 내부 서버 오류 대신 적절한 응답을 반환하도록 하기


<a name="footnote_1">1</a>: 이 페이지는
[FastAPI todo list with authentication — Todo with User (6/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-apirouter-5-6-7042fbc1e9e3)를 편역한 것임.
