# 인증을 포함한 FastAPI Todo 목록 <sup>[1](#footnote_1)</sup>

이 포스팅에서는 인기 있는 Python 웹 프레임워크인 FastAPI를 사용해 강력하고 안전한 todo 목록을 구축하는 방법을 살펴본다. 작업 관리를 위한 경량의 고성능 솔루션을 찾고 있다면 RESTful API에 대한 기본 지원과 효율적인 데이터 유효성 검사 시스템을 갖춘 FastAPI가 이상적인 선택이 될 것이다.

이 튜토리얼에서는 경로와 엔드포인트를 정의하고, 사용자 인증을 처리하고, 데이터베이스와 통합하여 todo 항목을 지속하는 방법을 포함하여 FastAPI로 RESTful API를 구축하는 기본 사항을 다룬다. 이 튜토리얼을 마치면 빠르고 안전하며 확장 가능한 완전한 기능의 todo 목록을 만들 수 있다.

1. [들어가며](./intro.md)
1. [데이터베이스](./database.md)
1. [CRUD](./crud.md)
1. [인증](./auth.md)
1. [APIRouter](./apirouter.md)
1. [사용자와 todo](./with-user.md)

<a name="footnote_1">1</a>: 이 페이지는
[FastAPI Todo List with Authentication — Intro (1/6)](https://python.plainenglish.io/fastapi-todo-list-with-authentication-1-c3b9c15f9753), 
[FastAPI Todo List with Authentication — Database (2/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-2-f020b337de08), 
[FastAPI Todo List with Authentication — CRUD (3/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-3-ce29a66fea4d), 
[FastAPI Todo List with Authentication — Auth (4/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-4-6e8dc3a07997),
[FastAPI Todo List with Authentication — ApiRouter (5/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-apirouter-5-6-7042fbc1e9e3) 및
[FastAPI Todo List with Authentication — Todo with User (6/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-todo-with-user-6-6-ce712bfa2599)를 편역한 것임.
