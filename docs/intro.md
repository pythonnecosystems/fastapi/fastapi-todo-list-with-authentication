# 들어가며 <sup>[1](#footnote_1)</sup>

숙련된 Python 개발자이든 웹 개발을 막 시작한 개발자이든 이 포스팅에서는 가장 까다로운 워크로드도 처리할 수 있는 강력한 작업 관리 시스템을 구축하는 데 필요한 도구를 제공한다. 이제 즐겨 사용하는 텍스트 편집기를 갖고 Python 환경을 실행한 다음 FastAPI로 todo 목록 작성을 시작해 보자.

> 환경을 설정하려면 몇 가지 필수 패키지를 설치해야 한다. 먼저 다음 명령을 사용하여 pip으로 FastAPI와 Uvicorn을 설치한다. (이전에 당연히 Python 가성환경을 설치하고 그 환경하에서 진행한다.)

```bash
(venv) $ pip install fastapi
(venv) $ pip install "uvicorn[standard]"
```

> 필요한 패키지를 설치한 후 종속성을 freeze하고 `requirements.txt` 파일에 저장해야 한다. 다음 명령을 사용한다.

```bash
(venv) $ pip freeze > requirements.txt
```

> 이렇게 하면 패키지 목록과 해당 버전이 포함된 `requirements.txt` 파일이 생성된다. 파일에 다음 패키지가 포함되어 있는지 확인하세요.

```
annotated-types==0.6.0
anyio==3.7.1
click==8.1.7
fastapi==0.104.1
h11==0.14.0
httptools==0.6.1
idna==3.6
pydantic==2.5.2
pydantic_core==2.14.5
python-dotenv==1.0.0
PyYAML==6.0.1
sniffio==1.3.0
starlette==0.27.0
typing_extensions==4.9.0
uvicorn==0.24.0.post1
uvloop==0.19.0
watchfiles==0.21.0
websockets==12.0
```

시작하려면 `main.py`를 열어보겠다. 첫 번째 단계는 todo를 저장할 목록을 만드는 것이다.

다음으로 CRUD 엔드포인트를 만들어야 한다. CRUD는 모든 데이터베이스 어플리케이션에 필요한 기본 작업인 Create, Read, Update 및 Delete의 약자이다.

todo의 경우 다음과 같은 엔드포인트가 필요하다.

- 새 todo 만들기
- 모든 todo 읽기
- 기존 todo 업데이트
- todo 삭제

> 다음은 이러한 엔드포인트에 대한 예이다.

```python
# app/main.py

from fastapi import FastAPI

app = FastAPI()

todo_list = []


@app.get("/todos")
async def get_todos():
    pass


@app.get("/todos/{todo_id}")
async def get_todo():
    pass


@app.post("/todos")
async def create_todo():
    pass


@app.post("/todos")
async def update_todo():
    pass


@app.delete("/todos/{todo_id}")
async def delete_todo():
    pass
```

이러한 리소스와 상호 작용하기 위해 RESTful API는 데이터베이스의 CRUD(Create, Read, Update, Delete) 작업에 매핑되는 GET, POST, DELETE 및 PATCH 같은 HTTP 메서드를 사용한다.

> GET: 이 HTTP 메서드는 리소스에서 데이터를 검색하거나 읽는 데 사용된다. RESTful API에서 GET 요청은 특정 리소스 또는 리소스 모음에서 데이터를 검색하는 데 사용할 수 있다.
>
> POST: 이 HTTP 메서드는 새 데이터 리소스를 만드는 데 사용된다. RESTful API에서 POST 요청은 새 리소스를 만들거나 기존 리소스에 데이터를 추가하는 데 사용할 수 있다.
> 
> DELETE: 데이터 리소스를 삭제하는 데 사용되는 HTTP 메서드이다. RESTful API에서 DELETE 요청은 특정 리소스 또는 리소스 컬렉션을 제거하는 데 사용할 수 있다.
> 
> PATCH: 이 HTTP 메서드는 데이터 리소스를 업데이트하거나 수정하는 데 사용된다. RESTful API에서 PATCH 요청은 기존 리소스를 부분적으로 업데이트하는 데 사용할 수 있다.

이러한 HTTP 메서드를 사용하여 RESTful API는 클라이언트가 서버와 상호 작용하고 데이터 리소스에 대해 CRUD 작업을 수행할 수 있는 간단하고 표준화된 방법을 제공한다. 따라서 개발자는 웹 브라우저에서 모바일 어플리케이션에 이르기까지 다양한 클라이언트가 사용할 수 있는 확장 가능하고 유연한 웹 서비스를 쉽게 구축할 수 있다.

어플리케이션을 실행하려면 `uvicorn main:app --reload` 명령을 사용한다. 기본적으로 어플리케이션은 `http://127.0.0.1:8000` 에서 실행된다.

먼저 모든 todo 만들고 검색하는 것부터 시작하겠다. 모든 todo를 검색하는 것은 리스트를 반환하면 되는 간단한 작업이다. 하지만 새 todo을 만들려면 몇 가지 추가 단계가 필요하다.

새 todo를 만들려면 `POST` HTTP 메서드를 사용하여 요청 본문에 새 todo에 대한 데이터를 전달한다. 이를 위해 fastapi 패키지의 `Body()` 함수를 사용하여 엔드포인트에 대한 요청 본문 스키마를 정의할 수 있다. 이 함수를 코드에 import하는 것을 잊지 마세요.

```python
# app/main.py

from fastapi import FastAPI, Body

# code ...

@app.get("/todos")
async def get_todos():
    return todo_list

@app.post("/todos")
async def create_todo(data = Body()):
    todo_list.append(data)
```

요청 본문을 통해 클라이언트로부터 데이터를 받을 수 있다는 점은 좋지만, 어떤 데이터를 수락할지 신중하게 결정해야 한다. 클라이언트가 예상하지 못한 데이터를 보내면 데이터 불일치 등 문제가 발생할 수 있다.

> 수신한 데이터가 유효하고 일관성이 있는지 확인하는 한 가지 방법은 `pydantic` 라이브러리를 사용하는 것이다. `pydantic`은 API 엔드포인트에 대한 데이터 스키마를 정의할 수 있는 데이터 유효성 검사 라이브러리이다. `pydantic`을 사용하면 데이터의 예상 형태를 정의하고 추가 처리 전에 데이터의 유효성을 검사할 수 있다.

> 다음은 `pydantic`을 사용하여 `POST` 요청으로 수신된 데이터의 유효성을 검사하는 방법의 예이다.

```python
from pydantic import BaseModel
from typings import Optional

# code ...

class Todo(BaseModel):
    id: Optional[int]
    value: str

@app.post("/todos")
async def create_todo(todo: Todo = Body()):
    new_todo = Todo(id=len(todo_list) + 1, value=data.value)
    todo_list.append(new_todo)
    return new_todo
```

이제 todo를 삭제해 보자. 이렇게 하려면 먼저 todo가 `todo_list`에 존재하는지 확인해야 한다. 존재한다면 리스트에서 제거한다. 존재하지 않는다면 `404 Not Found` 응답을 반환한다.

```python
from fastapi import FastAPI, Body, Response, status, HTTPException

# code ...

@app.delete("/todos/{todo_id}")
async def delete_todo(todo_id: int):
    for index, todo in enumerate(todo_list):
        if todo.id == todo_id:
            todo_list.pop(index)
            return Response(status_code=status.HTTP_204_NO_CONTENT)

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail={
            "message": f"Todo [{todo_id}] not found",
            "code": "TODO_NOT_FOUND",
            "status_code": status.HTTP_404_NOT_FOUND
        }
    )
```

> 응답에 HTTP 상태 코드를 지정하지 않으면 반환되는 기본 상태 코드는 `200`이다.

이제 todo를 업데이트해 보겠다. todo를 만들 때와 마찬가지로 `pydantic`을 사용하여 데이터의 유효성을 검사해야 한다. 또한 todo가 todo 리스트에 존재하는지 확인해야 한다. 존재한다면 todo의 값 필드를 새 값으로 업데이트한다. 존재하지 않는다면 `404 Not Found` 응답을 반환한다.

```python
@app.patch("/todos")
async def update_todo(data: Todo = Body()):
    for todo in todo_list:
        if todo.id == data.id:
            todo.value = data.value
            return Response(status_code=status.HTTP_204_NO_CONTENT)

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail={
            "message": f"Todo [{data.id}] not found",
            "code": "TODO_NOT_FOUND",
            "status_code": status.HTTP_404_NOT_FOUND
        }
    )
```

마지막으로, `id`로 단일 todo를 가져오는 엔드포인트를 추가해 보겠다. 경로에 매개변수로 `todo_id`를 지정해야 한다. 그런 다음 `todo_list`을 반복하여 찾아 일치하는 `id`를 가진 todo를 반환하면 된다. todo를 찾을 수 없으면 `404 Not Found` 응답을 반환한다.

```python
@app.get("/todos/{todo_id}")
async def get_todo(todo_id: int):
    for todo in todo_list:
        if todo.id == todo_id:
            return todo
        
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail={
            "message": f"Todo [{todo_id}] not found",
            "code": "TODO_NOT_FOUND",
            "status_code": status.HTTP_404_NOT_FOUND
        }
    )
```

이래 코드가 최종 `main.py`이다.

```python
from fastapi import FastAPI, Body, Response, status, HTTPException
from typing import Optional
from pydantic import BaseModel

app = FastAPI()


class Todo(BaseModel):
    id: Optional[int]
    value: str


todo_list = []


@app.get("/todos")
async def get_todos():
    return todo_list


@app.get("/todos/{todo_id}")
async def get_todo(todo_id: int):
    for todo in todo_list:
        if todo.id == todo_id:
            return todo

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail={
            "message": f"Todo [{todo_id}] not found",
            "code": "TODO_NOT_FOUND",
            "status_code": status.HTTP_404_NOT_FOUND
        }
    )


@app.post("/todos")
async def create_todo(data: Todo = Body()):
    new_todo = Todo(value=data.value)
    todo_list.append(new_todo)
    return new_todo


@app.post("/todos")
async def update_todo(data: Todo = Body()):
    for todo in todo_list:
        if todo.id == data.id:
            todo.value = data.value
            return Response(status_code=status.HTTP_204_NO_CONTENT)

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail={
            "message": f"Todo [{data.id}] not found",
            "code": "TODO_NOT_FOUND",
            "status_code": status.HTTP_404_NOT_FOUND
        }
    )


@app.delete("/todos/{todo_id}")
async def delete_todo(todo_id: int):
    for index, todo in enumerate(todo_list):
        if todo.id == todo_id:
            todo_list.pop(index)
            return Response(status_code=status.HTTP_204_NO_CONTENT)

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail={
            "message": f"Todo [{todo_id}] not found",
            "code": "TODO_NOT_FOUND",
            "status_code": status.HTTP_404_NOT_FOUND
        }
    )
```

다음 포스팅에서는 코드를 단순한 리스트가 아닌 데이터베이스에 연결하여 한 단계 더 발전시켜 본다. 또한 자동 생성된 Swagger 문서의 강력한 기능 등에 대해서도 살펴볼 예정이다.


<a name="footnote_1">1</a>: 이 페이지는
[FastAPI Todo List with Authentication — Intro (1/6)](https://python.plainenglish.io/fastapi-todo-list-with-authentication-1-c3b9c15f9753)를 편역한 것임.
