# 데이터베이스 <sup>[1](#footnote_1)</sup>

**FastAPI** todo 목록 어플리케이션의 다음 단계로 코드를 데이터베이스에 연결해 보겠다. 구체적으로는 Postgres 데이터베이스 관리 시스템과 Python 측의 **SQLmodel** ORM 라이브러리와 **asyncpg**를 사용할 것이다.

이 시리즈의 이전 포스팅을 잘 이해하셨다고 가정하고 거기서 작성한 코드를 기반으로 작성해 보겠다. 아직 읽어보지 않으셨다면 [다음 링크](./intro.md)에서 찾아볼 수 있다.

> 코드와 데이터베이스의 통합을 시작하기 전에 필요한 종속성을 설치해야 한다. FastAPI 어플리케이션에서 SQLmodel과 asyncpg를 사용하려면 터미널에서 다음 명령을 실행하여야 한다.

```bash
(venv) $ pip install sqlmodel
(venv) $ pip install asyncpg
```

FastAPI 어플리케이션에 대한 데이터베이스 자격 증명(credential)을 설정하려면 프로젝트의 루트 디렉터리에 `.env` 파일을 만들면 된다. 이 파일에는 데이터베이스 비밀번호와 같은 민감한 정보가 저장되므로 이 파일을 안전하게 보관하고 공개적으로 공유하지 않는 것이 중요하다.

```
DB_NAME=todo
DB_PASSWORD=123456
DB_USERNAME=postgres
```

> 주의할 점은 `DB_PASSWORD`를 Postgres 데이터베이스에 설정한 실제 비밀번호로 바꿔야 한다는 것이다.

FastAPI 어플리케이션을 Postgres 데이터베이스에 연결하려면 sqlmodel 및 asyncpg를 사용하여 데이터베이스 연결을 설정하는 코드가 포함된 `database.py` 파일을 만들어야 한다.

다음은 `database.py` 파일의 예시이다.

```python
# app/database.py

from sqlmodel import SQLModel

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from dotenv import dotenv_values

config = dotenv_values("./.env")
username = config.get("DB_USERNAME")
password = config.get("DB_PASSWORD")
dbname = config.get("DB_NAME")

# Create the database connection URL using our credentials from the .env file
SQLALCHEMY_DATABASE_URL = f"postgresql+asyncpg://{username}:{password}@localhost:5432/{dbname}"

# Create the engine using the URL and enable future support and echoing of SQL statements
engine = create_async_engine(SQLALCHEMY_DATABASE_URL, echo=True, future=True)

# Define an async function to initialize our database
async def init_db():
    async with engine.begin() as conn:
        await conn.run_sync(SQLModel.metadata.drop_all)
        await conn.run_sync(SQLModel.metadata.create_all)


# Create an async session maker to use in our FastAPI application
async_session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)
```

FastAPI는 어플리케이션을 시작할 때 원하는 코드를 실행할 수 있는 훌륭한 기능을 제공한다. 이 기능을 활용하여 데이터베이스 연결을 초기화하고 필요한 테이블을 만들 수 있다.

`app/main.py` 파일에 다음 코드를 추가한다.

```python
@app.on_event("startup")
async def on_startup():
    await init_db()
```

Todo에 대한 데이터베이스 모델을 정의하기 위해 `todo.py`라는 새 파일을 만들고 다음과 같이 정의한다.

```python
# app/models/todo.py

import datetime
from typing import Optional

from sqlmodel import SQLModel, Field

import sqlalchemy as sa


class Todo(SQLModel, table=True):
    __tablename__ = "todos"

    id: Optional[int] = Field(default=None, primary_key=True)
    value: str = Field(sa_column=sa.Column(sa.TEXT, nullable=False, unique=True))
    created_at: datetime.datetime = Field(sa_column=sa.Column(sa.DateTime(timezone=True), datetime.datetime.now))
```

FastAPI에서는 종속성 주입을 사용하여 서비스를 생성하고 관리할 수 있다. 시작하기 위해 `deps.py`라는 파일을 만들고 데이터베이스에서 `TodoService` 인스턴스를 검색하는 종속성을 정의하겠다. 하지만 다음 단계에서 `TodoService` 클래스를 구현할 것이다. 코드는 다음과 같다.

```python
# app/deps.py

from services.todoService import TodoService
from database import async_session

async def get_todo_service():
    async with async_session() as session:
        async with session.begin():
            yield TodoService(session)
```

create_todo 메서드로 **TodoService** 구현을 시작하겠다. 프론트엔드에서 서비스로 전달될 데이터를 가져오기 때문에 잠재적인 오류로부터 자신을 보호하기 위해 **Pydantic** 유효성 검사를 사용하고자 한다. 따라서 요청 본문 정의가 포함될 `createTodoRequest.py`를 만들겠다.

```python
# app/requests/createTodoRequest.py

from pydantic import BaseModel


class CreateTodoRequest(BaseModel):
    value: str
```

다음 코드는 `todoService.py` 파일이다.

```python
# app/services/todoService.py

from sqlmodel import Session

from models.todo import Todo
from requests.createTodoRequest import CreateTodoRequest


class TodoService:
    def __init__(self, session: Session):
        self.session = session

    async def create_todo(self, data: CreateTodoRequest):
        new_todo = Todo(value=data.value)

        self.session.add(new_todo)
        await self.session.commit()

        return new_todo
```

상황이 조금 복잡해 질 것이다. 따라서 현재 프로젝트 폴더 구조를 보인다.

```
todo-list
├── .env
├── .git
├── .gitignore
├── LICENSE
├── README.md
├── app
│   ├── __init__.py
│   ├── database.py
│   ├── deps.py
│   ├── main.py
│   ├── models
│   │   └── __init__.py
│   │   └── todo.py
│   ├── requests
│   │   └── __init__.py
│   │   └── createTodoRequest.py
│   ├── services
│   │   └── __init__.py
│   │   └── todoService.py
│   └── tests
│       └── __init__.py
├── requirements.txt
└── venv
```

```python
# ?

from fastapi import FastAPI, Body, Response, status, HTTPException, Depends

# imports ...

from deps import get_todo_service
from requests.CreateTodoRequest import CreateTodoRequest
from services.TodoService import TodoService


# code ...

@app.post("/todos", response_model=Todo)
async def create_todo(*, data: CreateTodoRequest = Body(), todo_service: TodoService = Depends(get_todo_service)):
    new_todo = await todo_service.create_todo(data)

    return new_todo
```

`response_model`은 API 엔드포인트의 응답 본문을 정의하는 데 사용되는 Pydantic 모델을 지정하는 FastAPI의 엔드포인트 데코레이터에 있는 매개 변수이다.

> 엔드포인트가 호출되면 FastAPI는 response 모델을 사용하여 응답 본문 데이터의 유효성을 검사하고 예상 구조에 맞는지 확인한다. 응답 데이터가 지정된 스키마와 일치하지 않으면 FastAPI는 유효성 검사 오류를 발생시킨다.

코드가 예상대로 작동하는지 테스트하는 것이 중요하다. 해보자!

#### screen shot 필요!

> 다음에서 나머지 엔드포인트를 데이터베이스에 연결하겠다.


<a name="footnote_1">1</a>: 이 페이지는
[FastAPI todo list with authentication — Database (2/6)](https://medium.com/@tomas.svojanovsky11/fastapi-todo-list-with-authentication-2-f020b337de08)를 편역한 것임.
